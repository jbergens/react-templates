import React from 'react';
import { render } from 'react-dom';
// import { configure } from 'mobx';
import App from './App';

render(<App />, document.getElementById('root'));
