/* Example using ES5 syntax with an object instead of a class */

import { action, observable, configure } from 'mobx';

// Use strict in MobX (mobx4 syntax)
configure({ enforceActions: 'observed' });

// Use mobx observable to create a store
// Using ES5 syntax. See counterStore.class.js for ES2015 syntax.
const counterStore = observable({
    counters: { a: 0, b: 0 },

    increase: action(function (key, increment) {
        console.log(`increaseAction was called. key: ${key}, increment: ${increment}`);

        const oldValue = this.counters[key];
        this.counters[key] = oldValue + (increment || 1);

        console.log('  new value: ', this.counters[key]);
    }),

    decrease: action(function (key, increment) {
        console.log(`decrease was called. key: ${key}, increment: ${increment}`);

        const oldValue = this.counters[key];
        this.counters[key] = oldValue + (increment || -1);

        console.log('  new value: ', this.counters[key]);
    }),
});
const CounterStore = counterStore;

export default CounterStore;
/**/
