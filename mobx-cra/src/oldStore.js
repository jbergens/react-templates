/* Testing class stores, not working code yet

class CounterStoreImpl {
    constructor() {
        // this.counters = { a: 0, b: 0 };
        extendObservable(this, {
            counters: { a: 0, b: 0 }
        })
    }
    // counters = { a: 0, b: 0 },

    increase(key, increment) {
        console.log(`increaseAction was called. key: ${key}, increment: ${increment}`);

        const oldValue = this.counters[key];
        this.counters[key] = oldValue + (increment || 1);

        console.log('  new value: ', this.counters[key]);
    }

    decrease(key, increment) {
        console.log(`decrease was called. key: ${key}, increment: ${increment}`);

        const oldValue = this.counters[key];
        this.counters[key] = oldValue + (increment || -1);

        console.log('  new value: ', this.counters[key]);
    }
}

*/
// const store = observable({
//     counters: { a: 0, b: 0 },
//     increase: action(key => increaseAction(key, 1)),
//     decrease: action(key => increaseAction(key, -1)),
// });

// function increaseAction(key, increment) {
//     console.log(`increaseAction was called. key: ${key}, increment: ${increment}`);
//
//     const oldValue = store.counters[key];
//     store.counters[key] = oldValue + (increment || 1);
//
//     console.log('  new value: ', store.counters[key]);
// }

// const actionCreators = {
//     increaseAction: key => increaseAction(key, 1), // Använder en funktion deklarerad ovan
//     decreaseAction: key => increaseAction(key, -1), // Använder en funktion deklarerad ovan
// };

// const store = new CounterStoreImpl();

// const CounterStore = observable(store);
// const CounterStore = observable(CounterStoreImpl);

// decorate(CounterStoreImpl, {
//     increase: action,
// });

// const CounterStore = CounterStoreImpl;
// export default CounterStore;
// export { actionCreators, store };
// export { CounterStore };
