import React, { Component } from 'react';
import { NICE, GOOD } from './colors';
import ConnectedCounterView from './ConnectedCounterView';
import PureCounter from './PureCounter';
import CounterToolbar from './CounterToolbar';
import './App.css';

function Intro() {
    return (
        <>
            <h1>Mobx demo 1</h1>
            <p className="App-intro">This app uses create-react-app with additions for mobx.
            Try to change the code to see that the dev server is working
            and then create your own components and stores.
            </p>
        </>
    );
}

export default class App extends Component {
    render() {
        return (
            <div>
                <Intro />
                <ConnectedCounterView name="Alfa" />
                <div>
                    The counters don&apos;t have to be next to each other.
                    <PureCounter name="Gamma" counter="a" color={GOOD} />
                    <CounterToolbar counter="a" />
                </div>

                <div>
                    <ConnectedCounterView name="Beta" counter="b" color={NICE} />
                    <PureCounter name="Delta" counter="b" color={NICE} />
                    <p>Buttons can also be placed anywhere on the page.</p>
                    <CounterToolbar counter="b" />
                </div>
            </div>
        );
    }
}
