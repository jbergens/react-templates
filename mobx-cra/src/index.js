// This must be the first line in src/index.js
import 'react-app-polyfill/ie9';
import React from 'react';
import { render } from 'react-dom';
// import { configure } from 'mobx';
import App from './App';

render(<App />, document.getElementById('root'));
