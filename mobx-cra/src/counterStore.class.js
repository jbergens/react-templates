/* Example use ES2015 class syntax

import { action, observable, decorate, configure } from 'mobx';

// Use strict in MobX (mobx4 syntax)
configure({ enforceActions: 'observed' });

// Use ES2015 class and then use mobx decorate below to create a store
class CounterStore {
    counters = {
      "a": 0, "b": 0
    }

    increase(key, increment) {
        console.log(`increaseAction was called. key: ${key}, increment: ${increment}`);

        const oldValue = this.counters[key];
        this.counters[key] = oldValue + (increment || 1);

        console.log('  new value: ', this.counters[key]);
    }

    decrease(key, increment){
        console.log(`decrease was called. key: ${key}, increment: ${increment}`);

        const oldValue = this.counters[key];
        this.counters[key] = oldValue + (increment || -1);

        console.log('  new value: ', this.counters[key]);
    }
}

decorate(CounterStore, {
    counters: observable,
    increase: action,
    decrease: action,
});

// Create a singleton instance  and export it
const counterStore = new CounterStore();

export default counterStore;
export { CounterStore }; // Also export the class for testing
*/
