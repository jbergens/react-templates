import { action, observable, configure } from 'mobx';

// Use strict in MobX (mobx4 syntax)
configure({ enforceActions: 'observed' });

// Use mobx observable to create a store
// Using ES5 syntax. See counterStore.class.js for ES2015 syntax.
const counterStore = observable({
    counters: { a: 0, b: 0 },

    increase(key, increment) {
        console.log(`increaseAction was called. key: ${key}, increment: ${increment}`);

        const oldValue = this.counters[key];
        this.counters[key] = oldValue + (increment || 1);

        console.log('  new value: ', this.counters[key]);
    },

    decrease(key, increment) {
        console.log(`decrease was called. key: ${key}, increment: ${increment}`);

        const oldValue = this.counters[key];
        this.counters[key] = oldValue + (increment || -1);

        console.log('  new value: ', this.counters[key]);
    },
}, {
    increase: action, // When strict/safe mode is enabled actions must be marked
    decrease: action, // When strict/safe mode is enabled actions must be marked
});

export default counterStore;
