import React from 'react';
import PropTypes from 'prop-types';
import { observer } from 'mobx-react';
// The "store" is more of a model
import counterStore from './counterStore';
import { NICE } from './colors';

const PureCounterImpl = ({ counter, color, name }) => {
    const key = counter;
    const myValue = counterStore.counters[key] || 0; // Use the model/counterStore

    console.log(`PureCounter[${key}].render()`,
        { counter, color, name });

    return (
        <div>
            <h1 style={{ color }}>
                PureCounter ({name}:{key}): {myValue}
            </h1>
        </div>
    );
};

PureCounterImpl.propTypes = {
    counter: PropTypes.string,
    color: PropTypes.string,
    name: PropTypes.string,
};

PureCounterImpl.defaultProps = {
    counter: 'a',
    color: NICE,
    name: null,
};

// Connect the counter to the mobx counterStore
const PureCounter = observer(PureCounterImpl); // Connect the component to MobX

export default PureCounter;
