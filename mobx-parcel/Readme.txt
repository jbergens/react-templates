# Mobx-parcel

This is a demo project showing a simple Mobx app. The project is created with the bundler Parcel and
contains a local development server. See below for details.

The code currently uses Mobx without decorators. Please see mobx-cra-dec for an example with decorators.

# To start developing

Start by running 
> yarn start

(or 'npm start' if you have installed npm)

# About Parcel

Parcel is a bundler for js projects. It is very easy to use and will automatically bundle all 
js-files referenced from the index.html.

Parcel contains a built in web server to use when developing and will automatically watch the
files on disk and re-build everything if some js-file is changes. 

