import React from 'react';
import PropTypes from 'prop-types';
import counterStore from './counterStore';
import CounterToolbarView from './CounterToolbarView';

// bind input to a function and avoid new lambda instances that degrade performance
const increaser = key => () => counterStore.increase(key);
const decreaser = key => () => counterStore.decrease(key);

const CounterToolbar = (props) => {
    const key = props.counter || 'a';

    return (
        <CounterToolbarView
            counter={key}
            increaseAction={increaser(key)}
            decreaseAction={decreaser(key)}
        />
    );
};
CounterToolbar.propTypes = {
    counter: PropTypes.string.isRequired,
};

export default CounterToolbar;
