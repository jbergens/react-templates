import React, { Component } from 'react';
import { NICE, GOOD } from './colors';
import ConnectedCounterView from './ConnectedCounterView';
import PureCounter from './PureCounter';
import CounterToolbar from './CounterToolbar';
import './App.css';

function Intro() {
    return (
        <React.Fragment>
            <h1>Mobx demo</h1>
            <p className="App-intro">This app uses create-react-app with additions for mobx.
            Try to change the code to see that the dev server is working
            and then create your own components and stores.
            </p>
        </React.Fragment>
    );
}

export default class App extends Component {
    render() {
        return (
            <div>
                <Intro />
                <ConnectedCounterView name="Alfa" />
                <ConnectedCounterView name="Arne" counter="c" />
                <ConnectedCounterView name="Aron" />
                <div>
                    The counters don&apos;t have to be next to each other.
                    <PureCounter name="Gamma" counter="a" color={GOOD} />
                    <CounterToolbar counter="a" />
                </div>

                <div>
                    <ConnectedCounterView name="Beta" counter="b" color={NICE} />
                    <PureCounter name="Delta" counter="b" color={NICE} />
                    <p>Buttons can also be placed anywhere on the page.</p>
                    <CounterToolbar counter="b" />
                </div>
                <ConnectedCounterView name="Aron" />
                <ConnectedCounterView name="Aron" />
                <ConnectedCounterView name="Aron" />
                <ConnectedCounterView name="Aron" />
                <ConnectedCounterView name="Aron" />
            </div>
        );
    }
}
